

# Spring Boot

This is a project using Spring Boot.
refer to tuto : [part1] (https://www.youtube.com/watch?v=Ke7Tr4RgRTs) and [part2](https://www.youtube.com/watch?v=ZA9WpK-lxXM)

*Data structure :: service, entity, dao

*Spring boot annotations :: @Repository, @RequestMapping, @Qualifier...

*Database connection AWS(RDS) with application properties profiles :: default, dev, prod

## Getting Started
### Prerequisites
### Installing

## Running the tests
### Break down into end to end tests
### And coding style tests

## Deployment
## Built With

* [SpringBoot](https://projects.spring.io/spring-boot/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [AWS(RDS)](https://aws.amazon.com/free/?sc_channel=PS&sc_campaign=acquisition_TH&sc_publisher=google&sc_medium=english_cloud_computing_hv_b&sc_content=aws_core_e&sc_detail=aws&sc_category=cloud_computing&sc_segment=188877297273&sc_matchtype=e&sc_country=TH&s_kwcid=AL!4422!3!188877297273!e!!g!!aws&ef_id=Wpam-QAAAJ2LWza8:20180525143651:s)

## Contributing
## Versioning
## Authors
## License
## Acknowledgments
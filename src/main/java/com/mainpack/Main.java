package com.mainpack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Main {
    //type psvm
    public static void main(String[] args) {

        SpringApplication.run(Main.class, args);
    }
}

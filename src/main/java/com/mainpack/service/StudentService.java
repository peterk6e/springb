package com.mainpack.service;

import com.mainpack.dao.FakeStudentDaoImpl;
import com.mainpack.dao.StudentDao;
import com.mainpack.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;

//to Access Dao

@Service
public class StudentService {

    @Autowired // find the class and instantiate and inject it.
    @Qualifier("mySql")
    private StudentDao studentDao;

    public Collection<Student> getAllStudents(){
        return this.studentDao.getAllStudents();
    }

    public Student getStudentById(int id){
        return this.studentDao.getStudentById(id);
    }

    public void removeStudentById(int id){
         this.studentDao.removeStudentById(id);
    }

    public void updateStudent(Student student){
        this.studentDao.updateStudent(student);
    }

    public void insertStudent(Student student){
        this.studentDao.insertStudent(student);
    }
}

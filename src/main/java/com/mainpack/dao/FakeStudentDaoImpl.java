package com.mainpack.dao;

import com.mainpack.entity.Student;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

//retrieve DATA from database

@Repository
@Qualifier("fakeData")
public class FakeStudentDaoImpl implements StudentDao {

    //fake data
    private static Map<Integer, Student> students;

    static{
        students = new HashMap<Integer, Student>(){
            {
                put(1,  new Student(1, "said", "science"));
                put(2,  new Student(2, "alek", "math"));
                put(3,  new Student(3, "anna", "english"));
            }
        };
    }

    @Override
    public Collection<Student> getAllStudents(){
        return this.students.values();
    }

    @Override
    public Student getStudentById(int id){
        return this.students.get(id);
    }

    @Override
    public void removeStudentById(int id){
         this.students.remove(id);
    }

    @Override
    public void updateStudent(Student student){
        Student s = students.get(student.getId());
        s.setCourse(student.getCourse());
        s.setName(student.getName());
        students.put(student.getId(), student);
    }

    @Override
    public void insertStudent(Student student){
        students.put(student.getId(), student);
    }
}
